/*
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.samples.apps.sunflower.adapters.SunflowerPagerAdapter
import com.google.samples.apps.sunflower.databinding.FragmentViewPagerBinding
import com.google.samples.apps.sunflower.tapmad.core.HomeViewModel
import com.google.samples.apps.sunflower.tapmad.core.fragments.SectionVideosTabTabFragmentHS
import com.google.samples.apps.sunflower.tapmad.core.models.SectionViewModel
import com.google.samples.apps.sunflower.tapmad.core.response.Tabs
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeViewPagerFragment : Fragment() {
    lateinit var adapter: SunflowerPagerAdapter

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        val tabLayout = binding.tabs
        val viewPager = binding.viewPager

//        viewPager.adapter = SunflowerPagerAdapter(this)
        adapter = SunflowerPagerAdapter(this)

        getDatas(viewPager, tabLayout)

        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)

        return binding.root
    }


    private fun getTabTitle(position: Int): String? {
        return when (position) {
            position -> adapter.mFragmentTitleList.get(position)
            else -> null
        }
    }


    @SuppressLint("FragmentLiveDataObserve")
    private fun getDatas(viewPager: ViewPager2, tabLayout: TabLayout) {
        val vm: HomeViewModel by viewModel()
        var fragment: Fragment

        vm!!.getMainPageData()
        vm.items.observe(this, Observer {
            for (tab: Tabs in it) {
                val vm: SectionViewModel by viewModel()
                fragment = SectionVideosTabTabFragmentHS(vm)
                adapter.addFragmentAndData(fragment, tab.TabName, tab.TabId)
//                (fragment as  SectionVideosTabTabFragmentHS).setSectionEntities(tab.Sections, tab.TabName)
                adapter.notifyDataSetChanged()
                viewPager.adapter = adapter


                // Set the icon and text for each tab
                TabLayoutMediator(tabLayout, viewPager) { tab, position ->
//            tab.setIcon(getTabIcon(position))
                    tab.text = getTabTitle(position)
                }.attach()

            }

        })

        handleEvent(viewPager)


    }

    private fun handleEvent(viewPager: ViewPager2) {
        viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                Log.e("Selected_Page", position.toString() + " " + adapter.getTabId(position))
                var fragment: Fragment = adapter.getItem(position)
                (fragment as SectionVideosTabTabFragmentHS).getDetailsData(adapter.getTabId(position))
            }

            override fun onPageScrollStateChanged(state: Int) {
                super.onPageScrollStateChanged(state)
            }
        })
    }
}