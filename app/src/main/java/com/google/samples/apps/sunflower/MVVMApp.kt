package com.google.samples.apps.sunflower

import android.app.Application
import com.leopold.mvvm.di.apiModule
import com.leopold.mvvm.di.networkModule
import com.leopold.mvvm.di.viewModelModule
import org.koin.android.ext.android.startKoin

/**
 * @author Leopold
 */
@Suppress("Unused")
class MVVMApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(
            networkModule,
            apiModule,
            viewModelModule
        ))
    }

}