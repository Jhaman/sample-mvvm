/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.tapmad.core

import androidx.fragment.app.Fragment
import com.google.samples.apps.sunflower.tapmad.core.response.Tabs
import com.leopold.mvvm.extension.with
import com.leopold.mvvm.util.NotNullMutableLiveData

class HomeViewModel(private val api: ServiceApi): BaseViewModel() {

    private val _refreshing: NotNullMutableLiveData<Boolean> = NotNullMutableLiveData(false)
    val refreshing: NotNullMutableLiveData<Boolean>
        get() = _refreshing

    private val _items: NotNullMutableLiveData<List<Tabs>> = NotNullMutableLiveData(arrayListOf())
    val items: NotNullMutableLiveData<List<Tabs>>
        get() = _items


    fun getMainPageData() {
        addToDisposable(api.getHomePageDetail("v1","en","android").with()
                .doOnSubscribe { _refreshing.value = true }
                .doOnSuccess { _refreshing.value = false }
                .doOnError { _refreshing.value = false }
                .subscribe({
                    _items.value = it.Tabs

//                    var fragment: Fragment
//                    for (tab: Tabs in it.Tabs) {
//                        fragment = SectionVideosTabFragmentHS()
//                        adapter.addFragmentAndData(fragment, tab.TabName)
//                        if (tab.Sections != null) (fragment as SectionVideosTabFragmentHS).setSectionEntities(
//                                tab.Sections,
//                                tab.TabName
//                        )
//                    }
                }, {
                    // handle errors
                })
        )
    }


}