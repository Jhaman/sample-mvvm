/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.tapmad.core

import com.tapmad.webservices.Wrapper.RetrofitWrapper
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface ServiceApi {

//    @GET("/search/repositories")
//    fun search(@QueryMap params: MutableMap<String, String>): Single<RepositoriesResponse>
//
//
    @GET("getFeaturedHomePageDetail/{version}/{locale}/{platform}")
    fun getHomePageDetail(@Path("version") version: String?,
                          @Path("locale") locale: String?,
                          @Path("platform") platform: String?): Single<RetrofitWrapper>


//    fun getHomePageDetailByTabId(
//            version: String,
//            locale: String,
//            platform: String,
//            TabId: Int
//    ): io.reactivex.Observable<RetrofitWrapper> {
//        return apiService.getHomePageDetailByTabId(version, locale, platform, TabId)
//    }


    @GET("getHomePageDetailByTabId/{version}/{locale}/{platform}/{TabId}")
    fun getHomePageDetailByTabId(
            @Path("version") version: String?,
            @Path("locale") locale: String?,
            @Path("platform") platform: String?,
            @Path("TabId") TabId: Int
    ): Single<RetrofitWrapper>

}