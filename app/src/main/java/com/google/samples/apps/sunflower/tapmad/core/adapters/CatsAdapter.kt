/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.tapmad.core.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.sunflower.R
import com.google.samples.apps.sunflower.databinding.NestedItemListBinding
import com.google.samples.apps.sunflower.databinding.NestedItemListCatsBinding
import com.google.samples.apps.sunflower.tapmad.core.BindingViewHolder
import com.google.samples.apps.sunflower.tapmad.core.models.SectionViewModel
import com.google.samples.apps.sunflower.tapmad.core.response.Categories
import com.google.samples.apps.sunflower.tapmad.core.response.VideoEntity


class CatsAdapter(var items: List<Categories> = arrayListOf(), val vm: SectionViewModel) :
        RecyclerView.Adapter<CatsAdapter.RepositoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        return RepositoryViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.nested_item_list_cats,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.binding.vm = vm
        holder.binding.categories = this.items[position]

    }

    override fun getItemCount() = items.size
    class RepositoryViewHolder(view: View) : BindingViewHolder<NestedItemListCatsBinding>(view)
}