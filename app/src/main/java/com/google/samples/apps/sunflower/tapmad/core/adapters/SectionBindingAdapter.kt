package com.google.samples.apps.sunflower.tapmad.core.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.sunflower.tapmad.core.models.SectionViewModel
import com.google.samples.apps.sunflower.tapmad.core.response.Section

/**
 * @author Leopold
 */
@BindingAdapter(value = ["sectionns", "viewModel"])
fun setSections(view: RecyclerView, items: List<Section>, vm: SectionViewModel) {
    view.adapter?.run {
        if (this is SectionsAdapter) {
            this.items = items
            this.notifyDataSetChanged()
        }
    } ?: run {
        SectionsAdapter(items, vm).apply { view.adapter = this }
    }
}