package com.google.samples.apps.sunflower.tapmad.core.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.samples.apps.sunflower.R
import com.google.samples.apps.sunflower.databinding.RecyclerMainItemsBinding
import com.google.samples.apps.sunflower.tapmad.core.BindingViewHolder
import com.google.samples.apps.sunflower.tapmad.core.models.SectionViewModel
import com.google.samples.apps.sunflower.tapmad.core.response.Section


/**
 * @author Leopold
 */
class SectionsAdapter(var items: List<Section> = arrayListOf(), val vm: SectionViewModel) :
    RecyclerView.Adapter<SectionsAdapter.RepositoryViewHolder>() {
    val VIEW_ITEM_CAT = 1
    val VIEW_ITEM_VIDEOS = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        if (viewType == VIEW_ITEM_CAT){
            return RepositoryViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.recycler_main_items,
                            parent,
                            false))

        }else {
            return RepositoryViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                            R.layout.recycler_main_items,
                            parent,
                            false
                    )
            )
        }
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.binding.item = this.items[position]
        holder.binding.vm = vm
        if (holder.binding.item!!.IsCategories){
            var adapter = CatsAdapter(vm = vm)
            holder.binding.nestedRecycler.adapter = adapter
            adapter.items = this.items[position].Categories
        }else{
            var adapter = VideosAdapter(vm = vm)
            holder.binding.nestedRecycler.adapter = adapter
            adapter.items = this.items[position].Videos
        }


//        holder.binding.nestedRecycler = .adapter = adapter

    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].IsCategories) VIEW_ITEM_CAT else VIEW_ITEM_VIDEOS
    }

    override fun getItemCount() = items.size
    class RepositoryViewHolder(view: View) : BindingViewHolder<RecyclerMainItemsBinding>(view)
}