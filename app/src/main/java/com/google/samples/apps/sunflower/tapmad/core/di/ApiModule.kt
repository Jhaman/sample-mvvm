package com.leopold.mvvm.di

import com.google.samples.apps.sunflower.tapmad.core.ServiceApi
import org.koin.dsl.module.module
import retrofit2.Retrofit

/**
 * @author Leopold
 */
val apiModule = module {
    single(createOnStart = false) { get<Retrofit>().create(ServiceApi::class.java) }
}