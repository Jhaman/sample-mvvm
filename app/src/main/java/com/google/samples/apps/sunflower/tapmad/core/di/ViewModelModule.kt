package com.leopold.mvvm.di

import com.google.samples.apps.sunflower.tapmad.core.HomeViewModel
import com.google.samples.apps.sunflower.tapmad.core.models.SectionViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * @author Leopold
 */
val viewModelModule = module {
//    viewModel { SearchViewModel(get(), get()) }
//    viewModel { BookmarkViewModel(get()) }
//    viewModel { MainActivityViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { SectionViewModel(get()) }
}