/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.tapmad.core.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.samples.apps.sunflower.R
import com.google.samples.apps.sunflower.adapters.GardenPlantingAdapter
import com.google.samples.apps.sunflower.databinding.FragmentGardenBinding
import com.google.samples.apps.sunflower.databinding.FragmentSectionsBinding
import com.google.samples.apps.sunflower.tapmad.core.BindingFragment
import com.google.samples.apps.sunflower.tapmad.core.HomeViewModel
import com.google.samples.apps.sunflower.tapmad.core.adapters.SectionsAdapter
import com.google.samples.apps.sunflower.tapmad.core.models.SectionViewModel
import com.google.samples.apps.sunflower.tapmad.core.response.Section
import org.koin.androidx.viewmodel.ext.android.viewModel

class SectionVideosTabTabFragmentHS(val sectionViewModel: SectionViewModel) : Fragment() {
    private lateinit var binding: FragmentSectionsBinding
//    val vm: SectionViewModel by viewModel()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSectionsBinding.inflate(inflater, container, false)
        val adapter = SectionsAdapter(vm = sectionViewModel)
        binding.gardenList.adapter = adapter

        subscribeUi(adapter, binding)
        return binding.root
    }



    private fun subscribeUi(adapter: SectionsAdapter, binding: FragmentSectionsBinding) {
        sectionViewModel.items.observe(viewLifecycleOwner) { result ->
            adapter.items = result
            adapter.notifyDataSetChanged()

        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


    fun getDetailsData( tabId:Int){
//        val vm: SectionViewModel by viewModel()
        sectionViewModel.getDetails(tabId)
    }


}