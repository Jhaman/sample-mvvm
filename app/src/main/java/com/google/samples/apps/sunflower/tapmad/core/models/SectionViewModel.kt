/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.samples.apps.sunflower.tapmad.core.models

import android.util.Log
import com.google.samples.apps.sunflower.tapmad.core.BaseViewModel
import com.google.samples.apps.sunflower.tapmad.core.ServiceApi
import com.google.samples.apps.sunflower.tapmad.core.response.Section
import com.leopold.mvvm.extension.with
import com.leopold.mvvm.util.NotNullMutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SectionViewModel(val api: ServiceApi): BaseViewModel() {
//    private val _itemsSections: NotNullMutableLiveData<List<Section>> = NotNullMutableLiveData(arrayListOf())
//    val items: NotNullMutableLiveData<List<Section>>
//        get() = _itemsSections

    private val _itemsSections: NotNullMutableLiveData<List<Section>> = NotNullMutableLiveData(arrayListOf())
    val items: NotNullMutableLiveData<List<Section>>
        get() = _itemsSections

    fun setSections(section: List<Section>){
        _itemsSections.value = section
    }


    fun getDetails(tabId: Int) {
        addToDisposable(api.getHomePageDetailByTabId("v1","en","android", tabId).with()
                .doOnSubscribe {}
                .doOnSuccess { }
                .doOnError { }
                .subscribe({
                    _itemsSections.value = it.Tabs.get(0).Sections

//                    var fragment: Fragment
//                    for (tab: Tabs in it.Tabs) {
//                        fragment = SectionVideosTabFragmentHS()
//                        adapter.addFragmentAndData(fragment, tab.TabName)
//                        if (tab.Sections != null) (fragment as SectionVideosTabFragmentHS).setSectionEntities(
//                                tab.Sections,
//                                tab.TabName
//                        )
//                    }
                }, {
                    // handle errors
                })
        )

    }


}