package    com.google.samples.apps.sunflower.tapmad.core.response



import android.os.Parcelable
import java.io.Serializable

data class Banner(
    var TabPosterPath: String,
    var VideoEntityId: Int,
    var TabURL: String,
    var IsPosterVideo: Boolean = false,
    var IsVideoChannel: Boolean = true,
    var GameRedirect: Boolean = false
) : Serializable