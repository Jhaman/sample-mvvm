package  com.google.samples.apps.sunflower.tapmad.core.response

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter


data class Categories(
    val VoDCategoryId: Int,
    val SectionId: Int,
    val CategoryName: String,
    val CategorythumbImage: String,
    val NewCategoryImage: String,
    val CategoryIsOnline: Boolean,
    val IsCategoryFree: Boolean,
    val IsSeason: Boolean,
    val IsVideoChannel: Boolean,
    val PackageName: String,
    val PackageIsFree: Boolean,
    val PackageProduct: Int,
    val PackagePrice: Int


)





