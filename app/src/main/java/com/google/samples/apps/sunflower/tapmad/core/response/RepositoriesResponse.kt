package  com.google.samples.apps.sunflower.tapmad.core.response

import com.google.gson.annotations.SerializedName

/**
 * @author Leopold
 */
data class RepositoriesResponse(
    @SerializedName("total_count") val totalCount: Int,
    @SerializedName("incomplete_results") val incompleteResults: Boolean
//    @SerializedName("items") val repositories: ArrayList<Repository>
)