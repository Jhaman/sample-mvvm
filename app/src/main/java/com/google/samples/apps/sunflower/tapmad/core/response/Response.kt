
package  com.google.samples.apps.sunflower.tapmad.core.response

data class Response (
    var responseCode: Int,
    var status:String,
    var message:String


)