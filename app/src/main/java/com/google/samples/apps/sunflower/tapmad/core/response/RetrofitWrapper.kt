package com.tapmad.webservices.Wrapper

import com.google.samples.apps.sunflower.tapmad.core.response.*


data class RetrofitWrapper(
        var Response: Response,
        var MobileNetwors: List<MobileNetwors>,
        var Ad: Advertisment,
        var Tabs: List<Tabs>,
        var Categories: List<VideoCategories>,
        var DateTime: DateTime,
        var Videos:List<Videos>


    )