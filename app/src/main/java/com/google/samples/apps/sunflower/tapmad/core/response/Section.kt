package  com.google.samples.apps.sunflower.tapmad.core.response



data class Section (
        val SectionId:Int,
        val SectionName:String,
        val IsSectionMore:Boolean,
        val Videos:List<VideoEntity>,
        val IsCategories:Boolean,
        val Categories:MutableList<Categories>
)