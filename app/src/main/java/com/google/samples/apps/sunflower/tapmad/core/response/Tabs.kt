package  com.google.samples.apps.sunflower.tapmad.core.response

import com.google.samples.apps.sunflower.tapmad.core.response.Section


data class Tabs(
        var TabId:Int,
        var TabName:String,
        var TabPosterPath:String,
        var Sections:MutableList<Section>,
        var Banners: List<Banner>


)