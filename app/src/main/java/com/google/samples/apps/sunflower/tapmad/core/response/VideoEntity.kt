package  com.google.samples.apps.sunflower.tapmad.core.response



data class VideoEntity(

     val VideoEntityId:Int,
     val IsVideoChannel:Boolean,
     val VideoName: String,
     val VideoDescription: String,
     val VideoImagePathLarge: String,
     val VideoImageThumbnail: String,
     val NewVideoImageThumbnail: String,
     val VideoCategoryImagePath: String,
     val VideoImagePath: String,
     val VideoCategoryId:Int,
     val VideoPackageId:Int,
     val VideoSeasonNo:Int,
     val VideoTotalViews: Long,
     val VideoRating:Long,
     val VideoAddedDate: String,
     val VideoDuration: String,
     val IsVideoFree:Boolean,
     val IsFavourite:Boolean,
     val IsComingSoon:Boolean,
     val VideoType:Int,
     val VideoQuality:Int,
     val VideoDownloadId: Long,
     val VideoLocalPath: String,
     val IsVideoOnline:Boolean,
     val VideoRssFeedURL: String,
     val VideoUpdatedDate: String,
     val VideoIsAllowedInternationally:Boolean,
     val VideoAllowCountryCodeList:Boolean ,
     val VideoCountryCodeList: String ,
     val VideoDownloadDate: String ,
     val VideoDownloadStatus:Int ,
     val VideoDownloadURL: String,
     val NewChannelThumbnailPath: String,
     val NewVideoOnDemandThumb: String,
     val NewCatchUpThumb: String,
     val PackageName: String,
     val PackageIsFree:Boolean ,
     val PackageProduct:Int,
     val PackagePrice:Int,
     val IsRadio:Boolean,
     val Event_key:Int


)